package main

import (
	"flag"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/google/go-cmdtest"
)

var update = flag.Bool("update", false, "replace test file contents with output")

func Test(t *testing.T) {
	ts, err := cmdtest.Read(".")
	if err != nil {
		t.Fatal(err)
	}
	ts.Commands["gocdk-blob"] = cmdtest.InProcessProgram("gocdk-blob", run)
	ts.Setup = func(rootdir string) error {
		// On Windows, convert "\" to "/" and add a leading "/":
		slashdir := filepath.ToSlash(rootdir)
		if os.PathSeparator != '/' && !strings.HasPrefix(slashdir, "/") {
			slashdir = "/" + slashdir
		}
		return os.Setenv("ROOTDIR_URL", "file://"+slashdir)
	}
	ts.Run(t, *update)
}
