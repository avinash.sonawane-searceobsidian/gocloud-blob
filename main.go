package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"

	"github.com/google/subcommands"
	"gocloud.dev/blob"

	_ "gocloud.dev/blob/fileblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"
)

func main() {
	os.Exit(run())
}

func run() int {
	subcommands.Register(&downloadCmd{}, "")
	subcommands.Register(&uploadCmd{}, "")
	log.SetFlags(0)
	log.SetPrefix("gocdk-blob: ")
	flag.Parse()
	return int(subcommands.Execute(context.Background()))
}

type downloadCmd struct{}

func (*downloadCmd) Name() string     { return "download" }
func (*downloadCmd) Synopsis() string { return "Output a blob to stdout" }
func (*downloadCmd) Usage() string {
	return `download <bucket URL> <key>

  Read the blob <key> from <bucket URL> and write it to stdout.

  Example:
    gocdk-blob download gs://mybucket my/gcs/file > foo.txt`
}

func (*downloadCmd) SetFlags(_ *flag.FlagSet) {}

func (*downloadCmd) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 2 {
		f.Usage()
		return subcommands.ExitUsageError
	}
	bucketURL := f.Arg(0)
	blobKey := f.Arg(1)

	bucket, err := blob.OpenBucket(ctx, bucketURL)
	if err != nil {
		log.Printf("Failed to open bucket: %v\n", err)
		return subcommands.ExitFailure
	}
	defer bucket.Close()

	reader, err := bucket.NewReader(ctx, blobKey, nil)
	if err != nil {
		log.Printf("Failed to read %q: %v\n", blobKey, err)
		return subcommands.ExitFailure
	}
	defer reader.Close()

	_, err = io.Copy(os.Stdout, reader)
	if err != nil {
		log.Printf("Failed to copy data: %v\n", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

type uploadCmd struct{}

func (*uploadCmd) Name() string     { return "upload" }
func (*uploadCmd) Synopsis() string { return "Upload a blob from stdin" }
func (*uploadCmd) Usage() string {
	return `upload <bucket URL> <key>

  Read from stdin and write to the blob <key> in <bucket URL>.

  Example:
    cat foo.txt | gocdk-blob upload gs://mybucket my/gcs/file`
}

func (*uploadCmd) SetFlags(_ *flag.FlagSet) {}

func (*uploadCmd) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) (status subcommands.ExitStatus) {
	if f.NArg() != 2 {
		f.Usage()
		return subcommands.ExitUsageError
	}
	bucketURL := f.Arg(0)
	blobKey := f.Arg(1)

	bucket, err := blob.OpenBucket(ctx, bucketURL)
	if err != nil {
		log.Printf("Failed to open bucket: %v\n", err)
		return subcommands.ExitFailure
	}
	defer bucket.Close()

	writer, err := bucket.NewWriter(ctx, blobKey, nil)
	if err != nil {
		log.Printf("Failed to write %q: %v\n", blobKey, err)
		return subcommands.ExitFailure
	}
	defer func() {
		if err := writer.Close(); err != nil && status == subcommands.ExitSuccess {
			log.Printf("closing the writer: %v", err)
			status = subcommands.ExitFailure
		}
	}()

	_, err = io.Copy(writer, os.Stdin)
	if err != nil {
		log.Printf("Failed to copy data: %v\n", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}
